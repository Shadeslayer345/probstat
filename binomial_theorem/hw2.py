""" Print the binomial expansion for (a+b)^n where n is 50 (or any number)
"""

def factorial(n):
  """Calculates a factorial for any given integer.

  For any integer (n) we multiply the initial number by itself decremented until
  we reach one:

  n! = n * (n-1) * (n-2) * (n-3) * ... * (n - (n-1))

  Args:
      n (int): The number from which we start our calculations

  Returns:
      int: The calculated vaue
  """
  if n == 0:
    return 1

  return n * (factorial(n - 1))

def binomial_coefficient(n, k):
  """Returns the number of combinations for the provided parameters.

  Each coefficient is the factorial of the number of possibilities we are
  choosing from (n) divided by the factorial of the number of unordered events
  we want to choose (k) multiplied by the difference between the two (n - k).

    n! / k!(n-k)!

  Args:
      n (int): The number of items we are choosing from
      k (int): The number of items we are choosing

  Returns:
      int: The number of combinations
  """
  return int(factorial(n) / (factorial(k) * factorial(n - k)))

def num_to_exponent(x):
  """Changes an integer into a string representation that appears as an exponent
  for printing purposes.

  Args:
      x (int): The number we wish to transform to an exponent

  Returns:
      str: A unicode representation of the number as an exponent
  """
  exponents = {
      '0': u'\u2070',
      '1': u'\xb9',
      '2': u'\xb2',
      '3': u'\xb3',
      '4': u'\u2074',
      '5': u'\u2075',
      '6': u'\u2076',
      '7': u'\u2077',
      '8': u'\u2078',
      '9': u'\u2079'
  }
  return ''.join(exponents.get(num) for num in str(x))

def format_term(coefficient, a_exponent, b_exponent):
  """Takes a binomial expansion term and formats it to print significant figures
        and exponents.

  Args:
      coefficient (int): Our coefficient, aka nCr
      exp_one (int): The exponent of a, aka (n - k)
      exp_two (int): The exponent of b, aka (k)

  Returns:
      str: Our term with unicode superscripts
  """
  term = ''

  if coefficient > 1:
    term = str(coefficient)
  if a_exponent > 0:
    if a_exponent == 1:
      term += 'a'
    else:
      term += 'a{0}'.format(num_to_exponent(a_exponent))
  if b_exponent > 0:
    if b_exponent == 1:
      term += 'b'
    else:
      term += 'b{0}'.format(num_to_exponent(b_exponent))

  return term

def binomial_theorem(n=50):
  """Calculates and displays the binomial expansion for any given n.

  Counting from k=0 to n each term is:
  The binomieal coefficient between the current count (k) and the number of
  terms (n), multiplied by variable a to the power of the difference between n
  and k, multiplied by variable b to the power of k:

  expanding (a+b)⁵⁰ each term is = bc(n,k) * a^(n-k) * b^(k)

  Args:
      n (int, optional): The number of terms we want to expand to
  """
  for k in range(n+1):
    if k == (n):
      print(format_term(binomial_coefficient(n, k), (n - k), k))
    else:
      print(format_term(binomial_coefficient(n, k), (n - k), k), end=' + ')

def main():
  """Driver for our script.
  """
  super_script = input('The binomial expansion for (a+b)^50 (Press Enter): ')
  print()

  if super_script.isdigit():
    binomial_theorem(int(super_script))
  else:
    binomial_theorem()

if __name__ == '__main__':
  main()

#### Output ####

# otto@macko:~/Bucket/probsat|⇒  python ncr.py
# The binomial expansion for (a+b)^50 (Press Enter): 50
#
# a⁵⁰ + 50a⁴⁹b + 1225a⁴⁸b² + 19600a⁴⁷b³ + 230300a⁴⁶b⁴ + 2118760a⁴⁵b⁵ +
# 15890700a⁴⁴b⁶ + 99884400a⁴³b⁷ + 536878650a⁴²b⁸ + 2505433700a⁴¹b⁹ +
# 10272278170a⁴⁰b¹⁰ + 37353738800a³⁹b¹¹ + 121399651100a³⁸b¹² +
# 354860518600a³⁷b¹³ + 937845656300a³⁶b¹⁴ + 2250829575120a³⁵b¹⁵ +
# 4923689695575a³⁴b¹⁶ + 9847379391150a³³b¹⁷ + 18053528883775a³²b¹⁸ +
# 30405943383200a³¹b¹⁹ + 47129212243960a³⁰b²⁰ + 67327446062800a²⁹b²¹ +
# 88749815264600a²⁸b²² + 108043253365600a²⁷b²³ + 121548660036300a²⁶b²⁴ +
# 126410606437752a²⁵b²⁵ + 121548660036300a²⁴b²⁶ + 108043253365600a²³b²⁷ +
# 88749815264600a²²b²⁸ + 67327446062800a²¹b²⁹ + 47129212243960a²⁰b³⁰ +
# 30405943383200a¹⁹b³¹ + 18053528883775a¹⁸b³² + 9847379391150a¹⁷b³³ +
# 4923689695575a¹⁶b³⁴ + 2250829575120a¹⁵b³⁵ + 937845656300a¹⁴b³⁶ +
# 354860518600a¹³b³⁷ + 121399651100a¹²b³⁸ + 37353738800a¹¹b³⁹ +
# 10272278170a¹⁰b⁴⁰ + 2505433700a⁹b⁴¹ + 536878650a⁸b⁴² + 99884400a⁷b⁴³ +
# 15890700a⁶b⁴⁴ + 2118760a⁵b⁴⁵ + 230300a⁴b⁴⁶ + 19600a³b⁴⁷ + 1225a²b⁴⁸ + 50ab⁴⁹ + b⁵⁰

